package com.example.pouyan.test.project.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author : Abolfazl Karimian
 * @date : 11/27/2023
 **/
@Entity(tableName = "tbl_posts")
data class Post(

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var id: Int,

    @ColumnInfo(name = "caption")
    var caption: String,

    @ColumnInfo(name = "commentsCount")
    var commentsCount: Int,

    @ColumnInfo(name = "likes")
    var likes: Int,

    @ColumnInfo(name = "picture")
    var picture: String,

    @ColumnInfo(name = "isLikedBefore")
    var isLikedBefore: Boolean = false,

    )
