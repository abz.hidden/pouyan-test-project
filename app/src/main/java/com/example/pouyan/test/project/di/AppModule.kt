package com.example.pouyan.test.project.di

import android.content.Context
import androidx.room.Room
import com.example.pouyan.test.project.data.db.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * @author : Abolfazl Karimian
 * @date : 11/27/2023
 **/
@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    /**
     * build room database
     */
    @Provides
    @Singleton
    fun provideRoomDB(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(context, AppDatabase::class.java, "post_db")
        .fallbackToDestructiveMigration()
        .allowMainThreadQueries()
        .build()



    @Provides
    @Singleton
    fun providePostListDao(
        db: AppDatabase
    ) = db.postListDao()


}
