package com.example.pouyan.test.project.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.pouyan.test.project.data.model.Post

/**
 * @author : Abolfazl Karimian
 * @date : 11/27/2023
 **/
@Database(entities = [Post::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun postListDao(): PostListDao

}