package com.example.pouyan.test.project.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author : Abolfazl Karimian
 * @date : 11/27/2023
 **/
@Entity(tableName = "tbl_comments")
data class Comment(

    @ColumnInfo(name = "commentId")
    @PrimaryKey(autoGenerate = true)
    var commentId: Int,

    @ColumnInfo(name = "postId")
    var postId: Int,

    @ColumnInfo(name = "description")
    var description: String,
)
