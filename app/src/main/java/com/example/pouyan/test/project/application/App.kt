package com.example.pouyan.test.project.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * @author : Abolfazl Karimian
 * @date : 11/27/2023
 **/
@HiltAndroidApp
class App : Application()