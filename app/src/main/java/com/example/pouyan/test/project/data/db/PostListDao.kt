package com.example.pouyan.test.project.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.pouyan.test.project.data.model.Comment
import com.example.pouyan.test.project.data.model.Post

/**
 * @author : Abolfazl Karimian
 * @date : 11/27/2023
 **/
@Dao
interface PostListDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: ArrayList<Post>)

    @Query("SELECT id,caption,likes,picture,isLikedBefore,COUNT(commentId) as commentsCount FROM tbl_posts LEFT JOIN tbl_comments ON tbl_posts.id = tbl_comments.postId GROUP BY id,caption,likes,picture,isLikedBefore")
    fun getPostsList():  ArrayList<Post>?

    @Query("SELECT * FROM tbl_posts WHERE id=:id")
    fun getPostDetail(id:Int): Post?

    @Query("UPDATE tbl_posts SET likes=:likes WHERE id = :id")
    fun updatePostsLike(likes: Int, id: Int)

    @Query("SELECT likes FROM tbl_posts WHERE id = :id")
    fun getPostsLike(id: Int) : Int

    @Query("INSERT INTO tbl_comments (description,postId) VALUES(:description,:id)")
    fun addComment(description: String,id: Int)

    @Query("SELECT * FROM tbl_comments WHERE postId=:id")
    fun getComments(id:Int): ArrayList<Comment>?
}